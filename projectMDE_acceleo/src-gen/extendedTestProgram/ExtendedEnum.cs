namespace Extended {
	// Type definitions
	public enum MyEnum {
		ONE = 1, TWO, THREE, FOUR, FIVE, EIGHT = 8, NINE
	}

	public enum ByteFlags : byte {
		F1 = 1, F2 = 1 << 1, F3 = 1 << 2, F4 = 1 << 3, F5 = 1 << 4, F6 = 1 << 5, F7 = 1 << 6, F8 = 1 << 7
	}
} 
