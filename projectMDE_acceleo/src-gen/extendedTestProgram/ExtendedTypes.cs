namespace Extended {
	// Type definitions
	public delegate void Formatter(string log);

	public interface ISerializable {
		// interface methods
		System.Byte[] Serialize();
		void Deserialize(System.Byte[] bytes);
	}

	public readonly struct MyReadonlyStruct {
	
		// members
		public readonly string value;
	
	}

	public class Base {
	
		// members
		public Extended.Base.Data data;
	
		// inner types
		public struct Data : Extended.ISerializable {
		
			// members
			public float value;
		
			// constructors
			public  Data(float value) {
				this.value = value;
			}
		
			// methods
			public System.Byte[] Serialize() {
				return System.BitConverter.GetBytes(value);
			}
		
			public void Deserialize(System.Byte[] bytes) {
				value = System.BitConverter.ToSingle(bytes, 0);
			}
		
		}
	
	}

	public class Derived : Extended.Base, Extended.ISerializable {
	
		// constructors
		public  Derived() {
			data = new Data(0.5f);
		}
	
		// methods
		public System.Byte[] Serialize() {
			return data.Serialize();
		}
	
		public void Deserialize(System.Byte[] bytes) {
			data.Deserialize(bytes);
		}
	
	}
} 
