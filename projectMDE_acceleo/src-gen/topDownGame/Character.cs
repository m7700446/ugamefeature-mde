namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class Character : UnityEngine.MonoBehaviour {
	
		// members
		public float speed;
		private float horizontal;
		public UnityEngine.Rigidbody2D rigidbody;
		private float vertical;
	
		// methods
		void Update() {
			horizontal = Input.GetAxisRaw("Horizontal");
			vertical = Input.GetAxisRaw("Vertical");
			rigidbody.velocity = new Vector2(horizontal * speed, vertical * speed);
			if (Input.GetKeyDown(KeyCode.P)) {
			rigidbody.AddForce(new Vector2(1,0)*10.0f, ForceMode2D.Impulse);
			}
		}
	
		void Start() {
		}
	
		public void OnTriggerEnter2D(UnityEngine.Collider2D collider) {
			if (collider.gameObject.tag == "Enemy") {
			DataManager.instance.SetLives(DataManager.instance.GetLives()-1);
			}
		}
	
	}
} 
