namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class BarDisplayLives : UnityEngine.MonoBehaviour {
	
		// members
		public float maxValue;
		public UnityEngine.UI.Image barImage;
	
		// methods
		void Update() {
			barImage.fillAmount =  DataManager.instance.GetLives()/maxValue;
		}
	
	}
} 
