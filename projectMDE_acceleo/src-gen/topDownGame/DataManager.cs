namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class DataManager : UnityEngine.MonoBehaviour {
	
		// members
		public static DSL.DataManager instance;
		public int lives;
	
		// methods
		private void Awake() {
			instance??= this;
		}
	
		public int GetLives() {
			return this.lives;
		}
	
		public void SetLives(int value) {
			this.lives = value;
		}
	
	}
} 
