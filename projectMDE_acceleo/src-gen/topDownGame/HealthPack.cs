namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class HealthPack : UnityEngine.MonoBehaviour {
	
		// methods
		void Update() {
		}
	
		void Start() {
		}
	
		public void OnTriggerEnter2D(UnityEngine.Collider2D collider) {
			if (collider.gameObject.tag == "Player") {
			DataManager.instance.SetLives(DataManager.instance.GetLives()+1);
			}
		}
	
	}
} 
