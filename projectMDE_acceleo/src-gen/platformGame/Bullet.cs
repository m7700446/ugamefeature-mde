namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class Bullet : UnityEngine.MonoBehaviour {
	
		// members
		public UnityEngine.Rigidbody2D rigidbody;
	
		// methods
		void Update() {
		}
	
		void Start() {
			rigidbody.AddForce(new Vector2(1,0)*0.0f, ForceMode2D.Impulse);
		}
	
		public void OnTriggerEnter2D(UnityEngine.Collider2D collider) {
			if (collider.gameObject.tag == "Enemy") {
			Destroy(gameObject);
			}
		}
	
	}
} 
