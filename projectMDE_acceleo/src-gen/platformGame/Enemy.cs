namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class Enemy : UnityEngine.MonoBehaviour {
	
		// methods
		void Update() {
		}
	
		void Start() {
		}
	
		public void OnTriggerEnter2D(UnityEngine.Collider2D collider) {
			if (collider.gameObject.tag == "Bullet") {
			DataManager.instance.SetScore(DataManager.instance.GetScore()+100);
			Destroy(gameObject);
			}
			if (collider.gameObject.tag == "PlayerFoot") {
			DataManager.instance.SetScore(DataManager.instance.GetScore()+100);
			Destroy(gameObject);
			}
		}
	
	}
} 
