namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class TextDisplayLives : UnityEngine.MonoBehaviour {
	
		// members
		public UnityEngine.UI.Text text;
		public string prefix;
		public string postfix;
	
		// methods
		void Update() {
			text.text = prefix + DataManager.instance.GetLives() +postfix;
		}
	
	}
} 
