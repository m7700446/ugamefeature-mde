namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class Character : UnityEngine.MonoBehaviour {
	
		// members
		public float speed;
		private float horizontal;
		public UnityEngine.Rigidbody2D rigidbody;
		private bool isGrounded;
		public float jumpIntensity;
		public UnityEngine.GameObject createdObject0;
	
		// methods
		void Update() {
			horizontal = Input.GetAxisRaw("Horizontal");
			if (Input.GetButtonDown("Jump") && isGrounded) {
						rigidbody.AddForce(new Vector2(0,1) * jumpIntensity);
						isGrounded = false;
						}
			rigidbody.AddForce(new Vector2(horizontal* speed, 0));
			if (Input.GetKeyDown(KeyCode.K)) {
			Instantiate(createdObject0, transform.position + new Vector3(1,0,0),createdObject0.transform.rotation);
			}
		}
	
		void Start() {
		}
	
		public void OnTriggerEnter2D(UnityEngine.Collider2D collider) {
			if (collider.gameObject.tag == "Enemy") {
			DataManager.instance.SetLives(DataManager.instance.GetLives()-1);
			}
		}
	
		public void OnCollisionEnter2D(UnityEngine.Collision2D collision) {
			if (collision.collider.gameObject.tag == "Floor"){
								isGrounded = true;
							}
		}
	
	}
} 
