namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class DataManager : UnityEngine.MonoBehaviour {
	
		// members
		public static DSL.DataManager instance;
		public int lives;
		public int score;
	
		// methods
		private void Awake() {
			instance??= this;
		}
	
		public int GetLives() {
			return this.lives;
		}
	
		public int GetScore() {
			return this.score;
		}
	
		public void SetLives(int value) {
			this.lives = value;
		}
	
		public void SetScore(int value) {
			this.score = value;
		}
	
	}
} 
