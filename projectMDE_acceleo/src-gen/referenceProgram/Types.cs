namespace Test {
	// Type definitions
	public interface MyInterface {
		// interface methods
		void Method1(float floaty);
		int Add(int adder);
	}

	public delegate string MyDelegate(string s);

	public class MyClass : Test.MyInterface {
	
		// members
		private Test.MyClass.InnerClass data;
	
		// constructors
		private static  MyClass() {
		}
	
		// methods
		public Test.MyClass.InnerClass GetData() {
		}
	
		// inner types
		private class InnerClass {
		
		}
	
	}

	public enum MyEnum {
		ONE = 1, TWO = 2, THREE
	}

	public readonly struct MyStruct {
	
	}

	public enum MyByteEnum : byte {
		s1 = 1, s2 = 1 << 1, s3 = 1 << 2, s4 = 1 << 3, s5 = 1 << 4, s6 = 1 << 5, s7 = 1 << 6, s8 = 1 << 7
	}
} 
