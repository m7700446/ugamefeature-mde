namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class Bullet : UnityEngine.MonoBehaviour {
	
		// members
		public UnityEngine.Rigidbody2D body;
		public int speed;
	
		// methods
		public void Awake() {
			body = gameObjects.GetComponent<Rigidbody2D>();
		}
	
		public void Start() {
			body.addForce(new Vector2(1, 0) * speed, ForceMode2D.Impulse);
		}
	
	}
} 
