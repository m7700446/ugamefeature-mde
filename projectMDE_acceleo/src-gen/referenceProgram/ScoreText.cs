namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	using UnityEngine.UI;
	// Type definitions
	public class ScoreText : UnityEngine.MonoBehaviour {
	
		// members
		public UnityEngine.UI.Text view;
		public string displayed;
	
		// methods
		public void Start() {
			view = gameObject.GetComponent<Text>();
		}
	
		public void Update() {
			view.text = displayed + DataManager.instance.score.getValue();
		}
	
	}
} 
