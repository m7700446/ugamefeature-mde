namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class DataManager : UnityEngine.MonoBehaviour {
	
		// members
		public static DSL.DataManager instance;
		public int score;
		public int initialScore;
		public int lives;
		public int initialLives;
	
		// methods
		public void Awake() {
			instance ??= this;
			score = new DataInt(initialScore);
			lives = new DataInt(initialLives);
		}
	
	}
} 
