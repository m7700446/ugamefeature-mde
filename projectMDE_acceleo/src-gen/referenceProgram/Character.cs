namespace DSL {
	// Type definitions
	public class Character : UnityEngine.MonoBehaviour {
	
		// members
		public UnityEngine.Rigidbody2D body;
		public float speed;
		public float jumpForce;
		public float horizontal;
		public bool isGrounded;
		public UnityEngine.GameObject objectCreated;
	
		// methods
		public void Update() {
			horizontal = Input.GetAxis("Horizontal");
			if (Input.GetButtonDown("Jump") && isGrounded) {
						isGrounded = false;
						body.AddForce(new Vector2(0, 1) * jumpforce, ForceMode2D.Impulse);
					}
			body.velocity = new Vector2(horizontal * speed, body.velocity.y);
			if (Input.GetKeyDown(KeyCode.K)) {
						Instantiate(objectCreated, transform.position + new Vector3(2, 0, 0), transform.rotation);
					}
		}
	
		public void OnCollisionEnter2D(UnityEngine.Collision2D collider) {
						if (collider.gameObject.tag == "Floor") {
									isGrounded = true;
								}
					}
	
		public void OnTriggerEnter2D(UnityEngine.Collider2D collider) {
									DataManager manager = DataManager.instance;
									if (collider.gameObject.tag == "Enemy") {
												manager.score.setValue(manager.score.getValue() + 100);
												Destroy(gameObject);
											}
								}
	
	}
} 
