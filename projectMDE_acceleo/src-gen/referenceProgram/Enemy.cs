namespace DSL {
	// using directives
	using UnityEngine;
	using System;
	// Type definitions
	public class Enemy : UnityEngine.MonoBehaviour {
	
		// methods
		public void OnTriggerEnter2D(UnityEngine.Collider2D collider) {
			DataManager manager = DataManager.instance;
			if (colider.gameObject.tag == "PlayerFoot") {
						manager.score.setValue(manager.score.getValue() + 100);
						Destroy(gameObject);
					}
			if (colider.gameObject.tag == "Bullet") {
						manager.score.setValue(manager.score.getValue() + 100);
						Destroy(gameObject);
					}
		}
	
	}
} 
